# 基础介绍
基于tp6的api生成模块,参考fastadmin的api模块
# 引用
下载到tp6的extend\jmwl下
```php
use  jmwl\realapidoc\Builder;
use  think\facade\View;
```
# 使用 
这里是所有的需要生成api的控制器路径啦、所以，你可以先获取到模块下的所有控制器
Builder(["\\app\\admin\\controller\\Demo","\\app\\admin\\controller\\Index"])
```php
            $title="类名";
        $url="http://real-think.jmwl51.com"; 
        //这里是所有的需要生成api的控制器路径啦
        $builder = new Builder(["\\app\\admin\\controller\\command"]);
        $content = $builder->render(root_path()."/extend/jmwl/realapidoc/template/index.html",$title,$url);
        // dump($content); die;
     
        View::assign('config',$content["config"]);
        View::assign('docslist',$content["docsList"]);
        View::assign('lang',$content["lang"]);
       return View();
```
# 视图
文件中的index.html就是视图啦，放到正确的位置
我放在了
app\admin\view\api\index.html 对应的控制器自然是 admin下的api控制器啦

# 控制器注释


|名称|	描述|	示例|
|-|-|-|
|@ApiSector	|API分组名称|	(测试分组)|
|@ApiRoute|	API接口URL，此@ApiRoute只是基础URL|	(/api/test)|
|@ApiInternal|	忽略的控制器,表示此控制将不加入API文档|	无|
|@ApiWeigh|	API方法的排序,值越大越靠前|	(99)|
# 控制器方法注释

|名称	|描述	|示例|
|-|-|-|
|@ApiTitle|	API接口的标题,为空时将自动匹配注释的文本信息	|(测试标题)|
|@ApiSummary|	API接口描述|	(测试描述)|
|@ApiRoute|	API接口地址,为空时将自动计算请求地址|	(/api/test/index)|
|@ApiMethod|	API接口请求方法,默认为GET|	(POST)|
|@ApiSector|	API分组,默认按钮控制器或控制器的@ApiSector进行分组|	(测试分组)|
|@ApiParams|	API请求参数,如果在@ApiRoute中有对应的{@参数名}，将进行替换|	(name="id", type="integer", required=true, description="会员ID")|
|@ApiHeaders|	API请求传递的Headers信息|	(name=token, type=string, required=true, description="请求的Token")|
|@ApiReturn	|API返回的结果示例	|({"code":1,"msg":"返回成功"})|
|@ApiReturnParams|	API返回的结果参数介绍|	(name="code", type="integer", required=true, sample="0")|
|@ApiReturnHeaders|	API返回的Headers信息|	(name="token", type="integer", required=true, sample="123456")|
|@ApiInternal	|忽略的方法,表示此方法将不加入文档|	无|
|@ApiWeigh|	API方法的排序,值越大越靠前|	(99)|


# 示范

```php
<?php

namespace app\api\controller;

/**
 * 测试API控制器
 */
class Test extends \app\common\controller\Api
{

    // 无需验证登录的方法
    protected $noNeedLogin = ['test'];
    // 无需要判断权限规则的方法
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     * 可以通过@ApiInternal忽略请求的方法
     * @ApiInternal
     */
    public function index()
    {
        return 'index';
    }

    /**
     * 私有方法
     * 私有的方法将不会出现在文档列表
     */
    private function privatetest()
    {
        return 'private';
    }

    /**
     * 测试方法
     *
     * @ApiTitle    (测试名称)
     * @ApiSummary  (测试描述信息)
     * @ApiSector   (测试分组)
     * @ApiMethod   (POST)
     * @ApiRoute    (/api/test/test/id/{id}/name/{name})
     * @ApiHeaders  (name=token, type=string, required=true, description="请求的Token")
     * @ApiParams   (name="id", type="integer", required=true, description="会员ID")
     * @ApiParams   (name="name", type="string", required=true, description="用户名")
     * @ApiParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据")
     * @ApiReturnParams   (name="code", type="integer", required=true, sample="0")
     * @ApiReturnParams   (name="msg", type="string", required=true, sample="返回成功")
     * @ApiReturnParams   (name="data", type="object", sample="{'user_id':'int','user_name':'string','profile':{'email':'string','age':'integer'}}", description="扩展数据返回")
     * @ApiReturn   ({
        'code':'1',
        'mesg':'返回成功'
     * })
     */
    public function test($id = '', $name = '')
    {
        $this->success("返回成功", $this->request->request());
    }

}
```
